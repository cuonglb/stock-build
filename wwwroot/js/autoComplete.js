﻿var AutoComplete = function () {
    this.Product = function (urlPost, controller, urlRedirect) {
        $("#" + controller).autocomplete({
            source: function (request, response) {
                $.post(urlPost, { Prefix: request.term }, function (data) {
                    response($.map(data, function (item) {
                        return item;
                    }));
                });
            },
            minLength: 1,
            autoFocus: true,
            select: function (event, ui) {
                $("#" + controller).val(ui.item.value);
                window.location.href = urlRedirect + ui.item.stockId;
                return false;
            }
        });
    };
};